import React from "react";
import {
  Route,
  Redirect
} from "react-router-dom";

import { systemAuth } from '../../dataAPI';

function AdminRoute({ component: Component, ...rest }) {
    return (
      <Route
        {...rest}
        render={props =>
          systemAuth.isAuthenticated && window.localStorage.getItem('_tg_type') === 'admin' ? (
            <Component {...props} />
          ) : (
              <Redirect to={{
                  pathname: "/login",
                  state: { from: props.location }
                }}
              />
            )
        }
      />
    );
  }

export default AdminRoute