import React from "react";
import { Redirect } from "react-router-dom";

import { apiLink } from '../../dataAPI';
import { HttpPOST } from '../../network';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

class CreateDeviceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fetched: {
                status: 'notSent'
            },
            device: {
                code: '',
                os: '',
                vendor: '',
                model: '',
                osVersion: '',
                image: '',
            }
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            device: { ...this.state.device, [name]: value }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        const token = window.localStorage.getItem('_tg_license');

        if (token || token !== 'undefined') {
            HttpPOST(apiLink + 'phones', JSON.stringify(this.state.device), token)
                .catch(error => {
                    this.setState({
                        fetched: { status: error.code }
                    });

                    throw error
                })
                .then(response => JSON.parse(response))
                .then(responseObject => {
                    console.log(responseObject);
                    this.setState({
                        fetched: { status: 201 }
                    });

                })
                .catch(error => {
                    this.setState({
                        fetched: { status: error.code }
                    });
                });
        }
    }

    ErrorHandler(props) {
        const status = props.status + '';
        const errorHandler = {
            'notSent': () => {
                return ''
            },
            '201': () => {
                return ''
            },
            '401': () => {
                return 'Invalid or no authentication token or not enough privileges'
            },
            'default': () => {
                return 'Unknown error. Please, try to create a phone again later'
            }
        }

        return errorHandler[status] ? errorHandler[status]() : errorHandler['default']();
    }

    render() {
        if (this.state.fetched.status !== 201) {
            return (
                <Form className="mt-5" onSubmit={this.handleSubmit}>
                    <Form.Group controlId="formCreateCode">
                        <Form.Control name="code" type="text" placeholder="Code*" required
                            value={this.state.device.codeValue}
                            onChange={this.handleInputChange} />
                    </Form.Group>
                    <Form.Group controlId="formCreateModel">
                        <Form.Control name="model" type="text"
                            placeholder="Model name*" required
                            value={this.state.device.modelValue} onChange={this.handleInputChange}
                        />
                    </Form.Group>
                    <Form.Group controlId="formCreateOS">
                        <Form.Control name="os" type="text"
                            placeholder="Operation system*" required
                            value={this.state.device.osValue}
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>
                    <Form.Group controlId="formCreateOSVersion">
                        <Form.Control name="osVersion" type="text"
                            placeholder="Operation system version"
                            value={this.state.device.osVersionValue}
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>
                    <Form.Group controlId="formCreateVendor">
                        <Form.Control
                            name="vendor" type="text"
                            placeholder="Vendor*" required
                            value={this.state.device.vendorValue}
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>
                    <Form.Group controlId="formCreateImage">
                        <Form.Control
                            name="image" type="text"
                            placeholder="Image URL"
                            value={this.state.device.imageValue}
                            onChange={this.handleInputChange}
                        />
                    </Form.Group>
                    <Button variant="primary" type="submit">Create</Button>
                    <Form.Text className="text-danger mt-3">
                        <this.ErrorHandler status={this.state.fetched.status} />
                    </Form.Text>
                </Form>
            );
        }

        return (<Redirect to="/devices-list" />);
    }
}

export default CreateDeviceForm;  