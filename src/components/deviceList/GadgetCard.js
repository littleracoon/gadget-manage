import React from "react";
import  { HttpPOST } from '../../network';
import { apiLink } from '../../dataAPI';
import noImage from '../../images/NO_IMG_600x600.png';
import Card from 'react-bootstrap/Card';
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';

/** returns gadget card */
class GadgetCard extends React.Component {

    /** returns the borrow button or the return button */
    ReturnBtn() {
        if (!this.props.card.borrowed) {
            return (
                <Button variant="primary" block
                    onClick={() => this.props.onClick(apiLink + 'phones/' + this.props.card.id + '/borrow')}
                >Borrow</Button>
            )
        }
        if ( window.localStorage.getItem('_tg_type') === 'admin' || 
            this.props.card.borrowed.user.id === window.localStorage.getItem('_tg_id') ) {
            return (
                <Button variant="secondary" block
                    onClick={() => this.props.onClick(apiLink + 'phones/' + this.props.card.id + '/return')}
                >Return</Button>
            )
        } 
        return (<div className="text-center py-2">Borrowed</div>)
    }

    render() {
        return (
            <Card style={{ height: '100%'  }}>
                <Card.Header className="d-flex align-items-center justify-content-center" style={{ height: 300 }}>
                    <Image
                        src={this.props.card.image ? this.props.card.image : noImage}
                        fluid
                        style={{ maxHeight: '100%' }}
                    />
                </Card.Header>
                <Card.Body className="bg-light d-flex flex-column">
                    <Card.Title>{this.props.card.model}</Card.Title>
                    <Card.Text className="mb-0">
                        <small>OS: {this.props.card.os} {this.props.card.osVersion}</small>
                    </Card.Text>
                    <Card.Text className="mb-0">
                        <small>Vendor: {this.props.card.vendor}</small>
                    </Card.Text>
                    <Card.Text className="mb-2">
                        {this.props.card.borrowed ?
                            (<small>{this.props.card.borrowed ? 'borrowed by: ' + this.props.card.borrowed.user.name : ''}</small>) : ' '}
                    </Card.Text>
                    <div className="mt-auto">
                        { this.ReturnBtn() }
                    </div>
                </Card.Body>
            </Card>
        );
    }

}

export default GadgetCard;