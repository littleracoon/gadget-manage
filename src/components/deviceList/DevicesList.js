import React from "react";
import { HttpGET, HttpPOST } from '../../network';
import { apiLink } from '../../dataAPI';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import Form from 'react-bootstrap/Form';
import Filter from './Filter';
import GadgetCard from './GadgetCard';

/** For acting with gadget list */
class DevicesList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fetched: false,
            idsForRendering: [],
            activeFilter: ''
        };
        this.responseObject = '';

        this.handleBtnClick = this.handleBtnClick.bind(this);
        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.handleSwichFilterChange = this.handleSwichFilterChange.bind(this);
    }

    componentDidMount() {
        const token = window.localStorage.getItem('_tg_license');

        if (token && token !== 'undefined') {
            this.fetchDevices(apiLink + 'phones', token);
        }
    }
    
    /** fetch devices from API */
    fetchDevices(url, token) {
        HttpGET(url, token)
            .then(response => {
                return JSON.parse(response)
            })
            .then(responseObject => {
                this.responseObject = responseObject.filter(elem => elem.code && elem.model && elem.os && elem.vendor);
                this.setState({
                    fetched: true,
                    idsForRendering: responseObject.map(elem => elem.id)
                })
            })
            .catch(error => console.log(error.message));
    }

    /** for borrowing and returning of device */
    handleBtnClick(url) {
        const token = window.localStorage.getItem('_tg_license');

        if (token && token !== 'undefined') {

            HttpPOST(url, '', token)
                .then(response => {
                    return JSON.parse(response)
                })
                .then(responseObj => {
                    console.log(responseObj);
                    this.fetchDevices(apiLink + 'phones', token);
                })
                .catch(error => console.log(error.message));
        }

    }

    /** gets filter field value */
    handleFilterChange(event, field) {
        const obj = this.responseObject;
        const response = event.target.value !== '_default' ? obj.filter(elem => elem[field] === event.target.value) : obj;

        this.setState({
            idsForRendering: response.map(elem => elem.id)
        })
    }

    /** renders gadget list cards */
    getGadgetsList(responseArr) {
        return responseArr.map((card, i) => {
            return (
                <Col lg="3" md="6" className="p-2" key={i}>
                    <GadgetCard card={card} onClick={(url) => this.handleBtnClick(url)} />
                </Col>
            );
        });
    }
    
    /** sets ids-array for filtering */
    getArrForFilter(field, arr, filteringArr) {
        if (filteringArr && filteringArr.length) {
            const filteredArr = arr.filter(elem => ~(filteringArr.indexOf(elem.id)))
            return filteredArr.map(elem => ({ id: elem['id'], [field]: elem[field] }))
        }
        return arr.map(elem => ({ id: elem['id'], [field]: elem[field] }))
    }

    getFilteredGadgetsList(arr, filterArr) {
        const filteredArr = arr.filter(elem => ~(filterArr.indexOf(elem.id)))
        return this.getGadgetsList(filteredArr);
    }

    /** returns switchers of filter fields */
    Switcher(props) {
        return (
            <div className="mb-3">
                <p>Select filter</p>
                <Form onChange={props.onChange}>
                    by OS <Form.Check inline type='radio' id="filter-os" data-value="os" name="switcher"/> 
                    By vendor <Form.Check inline type='radio' id="filter-vendor" data-value="vendor" name="switcher"/> 
                </Form>
            </div>
        )
    }

    /** sets the current filter field */
    handleSwichFilterChange(event) {
        this.setState({
            activeFilter: event.target.dataset.value
        })
    }

    /** returns gadget list and the adds for it */
    render() {
        return (
            <Container className="pt-4">
                <Row>
                    <Col lg="3" xs="12">
                        {this.state.fetched ? (<this.Switcher onChange={(event) => this.handleSwichFilterChange(event)}/>) : ''}
                    </Col>
                </Row>
                <Row>
                    <Col lg="3" xs="12">
                        {this.state.fetched && this.state.activeFilter ?
                            (<Filter
                                controlId={'filterBy' + this.state.activeFilter}
                                field={this.state.activeFilter}
                                arr={this.getArrForFilter(this.state.activeFilter, this.responseObject)}
                                onChange={(event) => this.handleFilterChange(event, this.state.activeFilter)}
                            ></Filter>) : ''}
                    </Col>
                </Row>
                <Row>
                    {this.state.idsForRendering.length ?
                        (this.getFilteredGadgetsList(this.responseObject, this.state.idsForRendering))
                        :
                        (<Spinner className="m-auto" animation="border" variant="primary" />)}
                </Row>
            </Container>
        );
    }
}

export default DevicesList;  