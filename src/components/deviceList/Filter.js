import React from "react";

import Form from 'react-bootstrap/Form';

/** For filtering of devices */
class Filter extends React.Component {

    /** returns an array of values of filter field */
    getFieldValues(field) {
        const arr = this.props.arr;
        const arr2 = arr.map(elem => elem[field]);
        const valuesArr = arr2.filter((item, pos) => arr2.indexOf(item) === pos && item);
        return valuesArr;
    }

    render() {
        return (
            <Form.Group controlId={this.props.fieldId}>
                <Form.Label >Filter by {this.props.field}</Form.Label>
                <Form.Control as="select" onChange={(event) => this.props.onChange(event)}>
                    <option value="_default">not selected</option>
                    {/* TODO */}
                    {this.getFieldValues(this.props.field).map((elem, i) => (<option value={elem} key={i}>{elem}</option>))}
                </Form.Control>
            </Form.Group>
        )
    }
}

export default Filter;