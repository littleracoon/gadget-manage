import React from 'react';

import { apiLink } from '../dataAPI';
import { GetAuth } from '../network';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

/** for logging in the system */
class SignInForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        login: '',
        password: '',
      },
      fetched: {
        status: 'notSent'
      }
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      user: { ...this.state.user, [name]: value }
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const token = window.localStorage.getItem('_tg_license');

    if (!token || token === 'undefined') {

      GetAuth(apiLink + 'login', JSON.stringify(this.state.user))
        .catch(error => {
          this.setState({
            fetched: { status: error.code }
          });

          throw error
        })
        .then(response => {
          return JSON.parse(response)
        })
        .then(dataToken => {
          window.localStorage.setItem('_tg_license', dataToken["token"]);
          window.localStorage.setItem('_tg_type', dataToken["type"]);
          window.localStorage.setItem('_tg_id', dataToken["id"]);
          window.localStorage.setItem('_tg_name', dataToken["name"]);
          window.localStorage.setItem('_tg_login', dataToken["login"]);
          console.log(dataToken);
          if (window.localStorage.getItem('_tg_license') && 
            window.localStorage.getItem('_tg_license') !== 'undefined') {
            this.props.login();
            this.setState({
              fetched: { status: 200 }
            });

          }
        })
        .catch(error => {
          this.setState({
            fetched: { status: error.code }
          });
        });
    }
  }

  ErrorHandler(props) {
    const status = props.status + '';
    const errorHandler = {
      'notSent': () => {
        return ''
      },
      '200': () => {
        return ''
      },
      '400': () => {
        return 'Login or password parameters are missing'
      },
      '401': () => {
        return 'Login or password are incorrect'
      },
      'default': () => {
        return 'Unknown error. Please, try signing in again'
      }
    }

    return errorHandler[status] ? errorHandler[status]() : errorHandler['default']();
  }

  render() {
    return (
      <Form className="mt-5" onSubmit={this.handleSubmit}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            name="login"
            type="email"
            placeholder="Enter email"
            value={this.state.user.login}
            onChange={this.handleInputChange}
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            name="password"
            type="password"
            placeholder="Password"
            value={this.state.user.password}
            onChange={this.handleInputChange}
          />
          <Form.Control.Feedback type="invalid"></Form.Control.Feedback>
        </Form.Group>

        <Button variant="primary" type="submit">Submit</Button>

        <Form.Text className="text-danger mt-3">
          <this.ErrorHandler status={this.state.fetched.status} />
        </Form.Text>
      </Form>
    );
  }
}

export default SignInForm;