import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
  } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { apiLink, systemAuth } from '../dataAPI';

/** Nav header */
function NavbarTop() {
  return (
    <Navbar bg="primary" variant="dark" expand="lg">
      <Container>
        <Link className="navbar-brand" to="/">MyDevices</Link>
        <AuthButton />
      </Container>
    </Navbar>
  )
}

export default NavbarTop;    