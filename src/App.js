import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import PrivateRoute from './components/customRoutes/PrivateRoute';
import AdminRoute from './components/customRoutes/AdminRoute';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';

import { systemAuth } from './dataAPI';

import DevicesList from './components/deviceList/DevicesList';
import CreateDeviceForm from './components/createDevice/CreateDeviceForm';
import SignInForm from './components/SignInForm';

class App extends React.Component {
  render() {
    return (
      <Router>
        <NavbarTop />
        
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <AdminRoute path="/create-device" component={CreateDevice} />
        <PrivateRoute path="/devices-list" component={DeviceListWrap} />

      </Router>
    )
  }
}

const Home = () => (<Redirect to="/devices-list" />);

const CreateDevice = () => {
  return (
    <Container className="pt-5">
      <h1>Create Device</h1>
      <Row className="justify-content-center align-items-lg-center">
        <Col xl="4" lg="5" md="6">
          <CreateDeviceForm />
        </Col>
      </Row>
    </Container>
  )
}

const DeviceListWrap = () => {
  return <DevicesList />
}

function NavbarTop() {
  return (
    <Navbar bg="primary" variant="dark" expand="lg">
      <Container>
        <Link className="navbar-brand col-sm-auto col-12" to="/">MyDevices</Link>
        {window.localStorage.getItem('_tg_type') === 'admin' ?
          (<Link className="btn btn-success ml-sm-auto" to="/create-device">Create device</Link>) : ''}
        <AuthButton />
      </Container>
    </Navbar>
  )
}

const AuthButton = withRouter(
  ({ history }) =>
    systemAuth.isAuthenticated ? (
      <div className="ml-3 my-auto">
        <Button
          variant="light" type="button"
          onClick={() => {
            systemAuth.signout(() => { history.push("/login"); localStorage.clear(); });
          }}
        >Sign out</Button>
      </div>
    ) : (
        <div className="ml-3 my-auto">
          <Link className="btn btn-light" to="/login">Sign in</Link>
        </div>
      )
);

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectToReferrer: false
    };
    this.login = this.login.bind(this);
  }

  login = () => {
    systemAuth.authenticate(() => {
      this.setState({ redirectToReferrer: true });
    });
  };

  render() {
    let { from } = this.props.location.state || { from: { pathname: "/devices-list" } };
    let { redirectToReferrer } = this.state;

    if (redirectToReferrer) return <Redirect to={from} />;

    return (
      <Container className="pt-5">
        <Row className="justify-content-center align-items-lg-center">
          <Col lg="6" >
            <h1 className="h4 text-center">For getting access to devices,<br /> please sign in</h1>
          </Col>
        </Row>

        <Row className="justify-content-center align-items-lg-center">
          <Col xl="4" lg="5" md="6">
            <SignInForm login={this.login} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;