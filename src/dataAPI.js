const apiLink = 'https://js-test-api.etnetera.cz/api/v1/';

/** a mark of autorisation */
const systemAuth = {
    isAuthenticated: (window.localStorage.getItem('_tg_license') && 
        window.localStorage.getItem('_tg_license') !== 'undefined' ? true : false),
    authenticate(cb) {
        this.isAuthenticated = true;
        setTimeout(cb, 100);
    },
    signout(cb) {
        this.isAuthenticated = false;
        setTimeout(cb, 100);
    }
}

export { apiLink, systemAuth };