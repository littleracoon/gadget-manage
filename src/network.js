/** for getting token */
function GetAuth(url, data) {

    return new Promise(function (resolve, reject) {

        const xhr = new XMLHttpRequest();

        xhr.open("POST", url, true);

        xhr.setRequestHeader("Content-Type", "application/json");

        xhr.onload = function () {
            if (this.status === 200) {
                resolve(this.response);
            } 
            else {
                var error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function () {
            reject(new Error("Network Error"));
        };

        xhr.send(data);
    });

};

export { GetAuth };

/** for GET requests */
function HttpGET(url, token) {

    return new Promise(function (resolve, reject) {

        const xhr = new XMLHttpRequest();

        xhr.open("GET", url, true);

        xhr.setRequestHeader("Auth-Token", token);
        xhr.setRequestHeader("Content-Type", "application/json");

        xhr.onload = function () {
            if (this.status === 200) {
                resolve(this.response);
            }  
            else {
                var error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function () {
            reject(new Error("Network Error"));
        };

        xhr.send();
    });

};

export { HttpGET };

/** for POST requests */
function HttpPOST(url, data, token) {

    return new Promise(function (resolve, reject) {

        const xhr = new XMLHttpRequest();

        xhr.open("POST", url, true);

        xhr.setRequestHeader("Auth-Token", token);
        xhr.setRequestHeader("Content-Type", "application/json");

        xhr.onload = function () {
            if (this.status === 200) {
                resolve(this.response);
            } 
            if (this.status === 201) {
                resolve(this.response);
            } 
            else {
                var error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function () {
            reject(new Error("Network Error"));
        };

        xhr.send(data);
    });

};

export { HttpPOST };